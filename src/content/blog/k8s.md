---
title: Kubernetes now available
pubDate: 2023-04-11
---

We now have a working [Kubernetes](https://kubernetes.io) cluster that any member can deploy onto.

As well as being able to deploy Pods, you can expose them over HTTPs at `<username>.tardis.ac/somepath`.

Applications made this way take priority over gitlab pages paths, so if you have your homepage at `john.tardis.ac`, you can put an app at `john.tardis.ac/app`.

To get started, read our [wiki page](https://wiki.tardisproject.uk/howto:learn_k8s). Provisioning is done automatically, so you can start using it immediately.
